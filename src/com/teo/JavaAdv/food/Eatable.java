package com.teo.JavaAdv.food;

public interface Eatable {

    int getKcal();

    int getProteinCount();

    int getCarbsCount();

    int getFatsCount();

    int getMaximumDosePerKg();

    String getRecipe();
}
