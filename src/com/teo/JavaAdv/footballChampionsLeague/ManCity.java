package com.teo.JavaAdv.footballChampionsLeague;


import com.teo.JavaAdv.footballChampionsLeague.clubInfoInterface.ClubInfo;
import com.teo.JavaAdv.footballChampionsLeague.enums.ClubName;

public class ManCity extends Best4TeamsInfo implements ClubInfo {


    public ManCity(String name, String country, Float valueOfTheSquad, String bestPlayer,
                   Integer numberOfChampionsLeagueTrophies, String nameOfTheCoach,
                   Boolean thisYearSemiFinalist) {

        super(name, country, valueOfTheSquad, bestPlayer,
                numberOfChampionsLeagueTrophies, nameOfTheCoach, thisYearSemiFinalist);
    }
    public ManCity(String teamName,String country){
        super(teamName, country, ClubName.MANCITY);
    }

    @Override
    public Float getMarketValueForTheBestPlayer() {
        return 60.9f;
    }

    @Override
    public String getStadium() {
        return "Etihad Stadium";
    }
    @Override
    public int getNumberOfSeatsInStadium(){
        return 53400;
    }
    @Override
    public int yearFounded(){
        return 1880;
    }


}