package com.teo.JavaAdv.footballChampionsLeague;


import com.teo.JavaAdv.footballChampionsLeague.clubInfoInterface.ClubInfo;

public class RealMadrid extends Best4TeamsInfo implements ClubInfo {

    public RealMadrid(String name, String country, Float valueOfTheSquad, String bestPlayer,
                      Integer numberOfChampionsLeagueTrophies, String nameOfTheCoach,
                      Boolean thisYearSemiFinalist) {
        super(name, country, valueOfTheSquad, bestPlayer,
                numberOfChampionsLeagueTrophies, nameOfTheCoach, thisYearSemiFinalist);
    }
    @Override
    public Float getMarketValueForTheBestPlayer() {
        return 64.9f;
    }
    @Override
    public String getStadium(){
        return "Santiago Bernabeu";
    }
    @Override
    public int getNumberOfSeatsInStadium(){
        return 81044;
    }

    @Override
    public int yearFounded(){
        return 1902;
    }
}
