package com.teo.JavaAdv.footballChampionsLeague.clubInfoInterface;


    public interface ClubInfo {
        String getStadium();
        int getNumberOfSeatsInStadium();
        int yearFounded();

    }

