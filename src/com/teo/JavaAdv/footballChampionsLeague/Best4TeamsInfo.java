package com.teo.JavaAdv.footballChampionsLeague;

import com.teo.JavaAdv.footballChampionsLeague.enums.ClubName;

public abstract class Best4TeamsInfo {
    private String teamName;
    private String country;
    private Float valueOfTheSquad;
    private String bestPlayer;
    private Integer numberOfChampionsLeagueTrophies;
    private String nameOfTheCoach;
    private Boolean thisYearSemiFinalist;



    abstract Float getMarketValueForTheBestPlayer();

    public Best4TeamsInfo() {
    }

    public Best4TeamsInfo(String name,String country, float valueOfTheSquad, String bestPlayer,
                          int numberOfChampionsLeagueTrophies, String nameOfTheCoach, boolean thisYearSemiFinalist) {
       this.teamName = name;
        this.country = country;
        this.valueOfTheSquad = valueOfTheSquad;
        this.bestPlayer = bestPlayer;
        this.numberOfChampionsLeagueTrophies = numberOfChampionsLeagueTrophies;
        this.nameOfTheCoach = nameOfTheCoach;
        this.thisYearSemiFinalist = thisYearSemiFinalist;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setValueOfTheSquad(float valueOfTheSquad) {
        this.valueOfTheSquad = valueOfTheSquad;
    }

    public void setBestPlayer(String bestPlayer){
        this.bestPlayer = bestPlayer;
    }

    public void setNumberOfChampionsLeagueTrophies(int numberOfChampionsLeagueTrophies){
        this.numberOfChampionsLeagueTrophies = numberOfChampionsLeagueTrophies;
    }

    public void setNameOfTheCoach(String nameOfTheCoach){
        this.nameOfTheCoach = nameOfTheCoach;
    }

    public void setThisYearSemiFinalist(boolean thisYearSemiFinalist){
        this.thisYearSemiFinalist =thisYearSemiFinalist;
    }

    public String getCountry(){
        return country;
    }

    public float getValueOfTheSquad() {
        return valueOfTheSquad;
    }

    public String getBestPlayer(){
        return bestPlayer;

    }

    public int getNumberOfChampionsLeagueTrophies() {
        return numberOfChampionsLeagueTrophies;
    }
    public String getNameOfTheCoach(){
        return nameOfTheCoach;
    }

    public boolean isThisYearSemiFinalist() {
        return thisYearSemiFinalist;
    }

    public String toString(){
        return "The best player of " + getTeamName() + " is " + getBestPlayer() + " and his Market value is " +
                getMarketValueForTheBestPlayer() + "Million Euro";
    }

    public String infoClub(){
        return getTeamName()+ " is the best Football team in "+
                getCountry()+". The value of the squad is " + getValueOfTheSquad()+
                " and the coach of the team is " + getNameOfTheCoach();
    }

    public Best4TeamsInfo(String teamName, String country, ClubName clubName) {
        this.teamName = teamName;
        this.country = country;
        this.clubName = clubName;
    }
    public ClubName clubName;
}
