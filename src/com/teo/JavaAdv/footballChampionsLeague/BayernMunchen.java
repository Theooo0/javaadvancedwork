package com.teo.JavaAdv.footballChampionsLeague;


import com.teo.JavaAdv.footballChampionsLeague.clubInfoInterface.ClubInfo;

public class BayernMunchen extends Best4TeamsInfo implements ClubInfo {

    public BayernMunchen(String name, String country, Float valueOfTheSquad, String bestPlayer,
                         Integer numberOfChampionsLeagueTrophies, String nameOfTheCoach,
                         Boolean thisYearSemiFinalist){
        super(name,country, valueOfTheSquad,bestPlayer,
                numberOfChampionsLeagueTrophies,nameOfTheCoach,thisYearSemiFinalist);




}
@Override
   public Float getMarketValueForTheBestPlayer(){
        return 50.0f;
}

    public String getStadium(){
        return "Allianz Arena";
    }

    @Override
    public int getNumberOfSeatsInStadium() {
        return 75024;
    }

    @Override
    public int yearFounded() {
        return 1900;
    }
}
