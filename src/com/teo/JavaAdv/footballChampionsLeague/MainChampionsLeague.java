package com.teo.JavaAdv.footballChampionsLeague;

import com.teo.JavaAdv.footballChampionsLeague.enums.ClubName;

public class MainChampionsLeague {
    public static void main(String[] args) {


        Best4TeamsInfo[] best4TeamsInfos = new Best4TeamsInfo[4];
        BayernMunchen bayern = new BayernMunchen("Bayern Munchen", "Germany", 1407.7f,
                "Robert Lewandowski", 6,
                "Julian Nagelsmann", false);
        best4TeamsInfos[0] = bayern;
        ManCity city = new ManCity("Manchester City", "England", 2688f, "Raheem Sterling", 1,
                "Pep Guardiola", true);
        best4TeamsInfos[1] = city;
        Psg paris = new Psg("Paris Saint Germain", "France", 808f, "Kylian Mbappe",
                0, "Mauricio Roberto Pochettino ",
                false);
        best4TeamsInfos[2] = paris;
        RealMadrid madrid = new RealMadrid("Real Madrid", "Spain",
                780f, "Thibaut Courtois", 13,
                "Carlo Ancelotti", true);
        best4TeamsInfos[3] = madrid;

        for (Best4TeamsInfo best : best4TeamsInfos) {
            System.out.println(best.toString());


        }
        for (Best4TeamsInfo best : best4TeamsInfos) {
            System.out.println(best.infoClub());
        }
        ClubName best = ClubName.MANCITY;
        System.out.println(best);
    }



}
