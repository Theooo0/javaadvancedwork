package com.teo.JavaAdv.footballChampionsLeague;


import com.teo.JavaAdv.footballChampionsLeague.clubInfoInterface.ClubInfo;

public class Psg extends Best4TeamsInfo implements ClubInfo {
    public Psg(String name, String country, Float valueOfTheSquad, String bestPlayer,
               Integer numberOfChampionsLeagueTrophies, String nameOfTheCoach,
               Boolean thisYearSemiFinalist) {
        super(name, country, valueOfTheSquad, bestPlayer, numberOfChampionsLeagueTrophies,
                nameOfTheCoach, thisYearSemiFinalist);
    }

    @Override
   public Float getMarketValueForTheBestPlayer() {
        return 159f;
    }

    @Override
    public String getStadium(){
        return "Le Parc des Princes";
    }

    @Override
    public int getNumberOfSeatsInStadium() {
        return 47929;
    }
    @Override
    public int yearFounded(){
        return 1970;
    }
}
