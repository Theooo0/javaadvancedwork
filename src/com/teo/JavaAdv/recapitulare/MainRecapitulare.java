package com.teo.JavaAdv.recapitulare;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * un program care reține informații despre figuri geometrice (cerc, patrat, triunghi)
 * <p>
 * citeste de la tastatura ce formă se va stoca
 * citeste de la tastatura informatii despre forma (de ex raza daca e cerc)
 * afiseaza aria tuturor formelor stocate
 * <p>
 * <p>
 * >> Ce formă doriti sa stocati?
 * Cerc
 * >> Dati raza cercului
 * 5
 * >> Aria cercului e 78
 * <p>
 * <p>
 * <p>
 * >> Ce formă doriti sa stocati?
 * Patrat
 * >> Dati latura patratului
 * 5
 * >> Aria patratului e 25
 */


public class MainRecapitulare {

    public static void main(String[] args) {

        Set<Forma> exampleSet = new TreeSet<>();
        Cerc c = new Cerc(100);
        Patrat p = new Patrat(6);
        Triunghi t = new Triunghi(6, 12);
        Patrat p2 = new Patrat(12);
        Patrat p3 = new Patrat(16);
        Triunghi t2 = new Triunghi(12, 24);

        // p aria = 36           p2 aria = 144
        System.out.println("p.compareTo(p2)");
        System.out.println(p.compareTo(p2));

        // this=p               form=t
        System.out.println("p.compareTo(t)");
        System.out.println(p.compareTo(t));

        // this=t               form=p
        System.out.println("t.compareTo(p)");
        System.out.println(t.compareTo(p));


        exampleSet.add(p3);
        exampleSet.add(p2);
        exampleSet.add(p);
        exampleSet.add(c);
        exampleSet.add(t2);
        exampleSet.add(t);






        String forma;

//        List<Forma> forme = new LinkedList<>();
//        Map<aria obiectului, obiect de tipul forma>
//        Map<Double, Forma> formeMap = new TreeMap<>();
        Set<Forma> formaTreeSet = new TreeSet<>();


        Scanner sc = new Scanner(System.in);
        while (1 == 1) {
            System.out.println("Ce forma doriti sa stocati?");
            forma = sc.nextLine();

            if (forma.equals("cerc")) {
                Cerc c1 = new Cerc();
                c1.initFromKeyboard(sc);
//                forme.add(c1);
//                formeMap.put(c1.getArea(), c1 );
                formaTreeSet.add(c1);
            } else if (forma.equals("patrat")) {
                Patrat p1 = new Patrat();
//                p1.initFromKeyboard2();
                p1.initFromKeyboard(sc);
//                forme.add(p1);
//                formeMap.put(p1.getArea(), p1);
                formaTreeSet.add(p1);
            } else if (forma.equals("triunghi")) {
                Triunghi t1 = new Triunghi();
                t1.initFromKeyboard(sc);
//                forme.add(t1);
//                formeMap.put(t1.getArea(), t1);
                formaTreeSet.add(t1);
            }
            /*for(int i = 0; i< forme.size(); i++){
                System.out.println(forme.get(i));
                System.out.println(forme.get(i).getArea());

            }*/
            // FOR (Obiect K în setul de valori formeMap.keySet())
//            for(Double k : formeMap.keySet()) {
//                System.out.println(formeMap.get(k));
//                System.out.println(formeMap.get(k).getArea());
//            }
            for (Forma forma1 : formaTreeSet) {
                System.out.println(forma1);
                System.out.println(forma1.getArea());
            }
        }


    }


}
