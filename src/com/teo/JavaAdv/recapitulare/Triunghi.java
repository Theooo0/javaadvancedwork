package com.teo.JavaAdv.recapitulare;

import java.util.Scanner;

public class Triunghi extends Forma{
    private double inaltime;
    private double baza;

    public Triunghi(){

    }

    public Triunghi(double inaltime,double baza){
        this.baza=baza;
        this.inaltime=inaltime;
    }

    public void initFromKeyboard(Scanner sc){
        System.out.println("Introduceti inaltimea triunghiului = ");
        this.inaltime = sc.nextDouble();
        System.out.println("Introduceti baza triunghiului = ");
        this.baza = sc.nextDouble();
        sc.nextLine();
    }
    @Override
    public double getArea() {
        return (baza*inaltime)/2;
    }
}
