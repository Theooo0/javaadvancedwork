package com.teo.JavaAdv.recapitulare;

import java.util.Scanner;

public class Cerc extends Forma{

    //public static String color = "black";
    double razaCerc=0;


    public Cerc(){
        this.razaCerc=0;
    }

    public Cerc(double razaCerc){
        this.razaCerc=razaCerc;
    }

    public void initFromKeyboard(Scanner sc) {
        System.out.println("Dati raza cercului:");
        // există în scanner "7\n"
        // nextDouble() citește doar "7"
        // deci în scanner a mai rămas "\n" necitit
        this.razaCerc = sc.nextDouble();
        sc.nextLine();
    }
    @Override
    public double getArea() {
        return Math.PI * (razaCerc * razaCerc);
    }
}
