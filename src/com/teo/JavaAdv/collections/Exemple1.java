package com.teo.JavaAdv.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Exemple1 {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);

        List<String> stringList = new ArrayList<>();

        System.out.println("Introduce 10 Strings: ");
        for (int i=0;i<10;i++){

            String inputFromConsole = scn.nextLine();
            stringList.add(inputFromConsole);
        }
        for (int i = 0; i< stringList.size();i++){
            System.out.println("The element from position [" + i + "] is " +stringList.get(i));
        }

    }
}
