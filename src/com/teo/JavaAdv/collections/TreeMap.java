package com.teo.JavaAdv.collections;

import com.teo.JavaAdv.animals.Dog;

import java.util.Map;

public class TreeMap {

    public static void main(String[] args) {
        Map<String, Dog> dogs2 = new java.util.TreeMap<>();
        Dog pluto = new Dog("Pluto", 1121);
        Dog rex = new Dog("Rex", 412);
        Dog max = new Dog("Max", 56211);
        Dog ares = new Dog("Ares", 2112);

        dogs2.put(pluto.getName(), pluto);
        dogs2.put(ares.getName(), ares);
        dogs2.put(max.getName(),max);
        dogs2.put(rex.getName(),rex);



        for (String name: dogs2.keySet()){
            System.out.println(dogs2.get(name));

        }
        for (Dog d : dogs2.values()){
            System.out.println(d.getId()+d.getName());
        }
    }
}
