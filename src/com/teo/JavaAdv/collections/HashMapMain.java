package com.teo.JavaAdv.collections;

import com.teo.JavaAdv.animals.Cat;
import com.teo.JavaAdv.animals.Dog;

import java.util.*;

public class HashMapMain {
    public static void main(String[] args) {
        Map<Integer, Dog> dogs = new LinkedHashMap<>();
        Dog pluto = new Dog("Pluto", 1121);
        Dog rex = new Dog("Ares", 2212);
        Dog max = new Dog("", 21211);
        Dog ares = new Dog("Rwex", 2112);

        dogs.put(pluto.getId(), pluto);
        dogs.put(2342, rex);
        dogs.put(max.getId(), max);
        dogs.put((ares.getId()), ares);


        Map<Integer, Cat> cats = new LinkedHashMap<Integer, Cat>();
        Cat tom = new Cat("Tom", 123212);
        Cat mitzi = new Cat("Mitzi", 3123);
        Cat eric = new Cat("Eric", 341);
        Cat sandra = new Cat("Sandra", 12334);

        cats.put(tom.getId(), tom);
        cats.put(mitzi.getId(), mitzi);
        cats.put(eric.getId(), eric);
        cats.put(sandra.getId(), sandra);


        int maxtId = Integer.MAX_VALUE;
        int minId = Integer.MIN_VALUE;

        for (Integer cheie : cats.keySet()) {
            System.out.print(cheie + " ");
            System.out.println(cats.get(cheie).getName());

        }


        for (Integer cheie : cats.keySet()) {
            if (cheie > maxtId) {
                maxtId = cheie;

            }

            System.out.println("Max id is: " +maxtId);
        }


        for (Integer cheie : cats.keySet()) {
            if (cheie > minId) {
                minId = cheie;

            }

        }System.out.println("Min id is: " +minId);
    }
}