package com.teo.JavaAdv.collections;

import com.teo.JavaAdv.animals.Dog;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetMain {
    public static void main(String[] args) {
        Set<Integer> intHashSet = new HashSet<Integer>();
        Set<Integer> intTreeSet = new TreeSet<Integer>();
        Set<Integer> linkedHashSet = new LinkedHashSet<Integer>();


        //afisare aleatorie
        intHashSet.add(-1);
        intHashSet.add(4);
        intHashSet.add(6);
        intHashSet.add(1);
        intHashSet.add(13);
        intHashSet.add(4);
        intHashSet.add(-2);
        intHashSet.add(0);

        //Afiseaza numerele in ordine crescatoare, tinand cont de semn(negativ sau pozitiv)

        intTreeSet.add(4);
        intTreeSet.add(5);
        intTreeSet.add(1);
        intTreeSet.add(13);
        intTreeSet.add(4);
        intTreeSet.add(-2);
        intTreeSet.add(0);

        //Afiseaza numerele in ordinea in care au fost introduse

        linkedHashSet.add(4);
        linkedHashSet.add(5);
        linkedHashSet.add(1);
        linkedHashSet.add(13);
        linkedHashSet.add(4);
        linkedHashSet.add(-2);
        linkedHashSet.add(0);


        for (Integer hash : intHashSet) {
            System.out.println("HashSet" + hash);
        }
        System.out.println();
        System.out.println();
        for (Integer tree : intTreeSet) {
            System.out.println("TreeSet: " + tree);
        }
        System.out.println();
        System.out.println();
        for (Integer linked : linkedHashSet) {
            System.out.println("LinkedSet: " + linked);
        }
    }

    Set<Dog>dogs = new TreeSet<>();

    Dog pluto = new Dog("Pluto", 1121);
    Dog rex = new Dog("Rex", 412);
    Dog max = new Dog("Max", 56211);
    Dog ares = new Dog("Ares", 2112);





}