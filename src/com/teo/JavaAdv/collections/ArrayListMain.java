package com.teo.JavaAdv.collections;

import java.util.ArrayList;
import java.util.List;

public class ArrayListMain {
    public static void main(String[] args) {


        String[] strings = new String[7];
        strings[0] = "zero";
        strings[1] = "one";
        strings[2] = "trei";
        strings[3] = "patru";
        strings[4] = "cinci";
        strings[5] = "sase";

        for (int i = strings.length-1;i>2;i--){
            strings[i] = strings[i-1];
        }
        strings[2]="doi";


        for (int i=0;i< strings.length;i++){
            System.out.println(strings[i]);
        }
        List<String> stringsArrayList =  new ArrayList<String>();
        System.out.println("stringArrayList size = " + stringsArrayList.size());
        stringsArrayList.add("Primul");
        stringsArrayList.add("Al doilea");
        stringsArrayList.add("Al patrulea");
        stringsArrayList.add(2,"Al treilea");

        System.out.println("stringArrayList size = " + stringsArrayList.size());
        System.out.println(stringsArrayList.get(0));
        System.out.println(stringsArrayList.get(2));

        for (int i=0; i<stringsArrayList.size();i++){
            System.out.println(stringsArrayList.get(i));
        }
    }
}