package com.teo.JavaAdv.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Exemplu3 {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        List<String> stringList = new ArrayList<>();

        System.out.println("Insert the elements!");

        String userInput = scn.nextLine();
        while (!userInput.equals( "The end")) {
            stringList.add(userInput);
            userInput = scn.nextLine();

        }
        for (int i = 0; i < stringList.size(); i++) {
            System.out.println(stringList.get(i));
        }


    }
}
