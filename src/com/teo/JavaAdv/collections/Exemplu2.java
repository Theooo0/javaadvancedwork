package com.teo.JavaAdv.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Exemplu2 {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        List<Integer> integerList = new ArrayList<>();
        int sum = 0;
        System.out.println("Introduce 7 elements");
        for (int i = 0; i < 8; i++) {
            System.out.println("Integer[" + i + "]= ");
            int input = scn.nextInt();
            integerList.add(i, input);
            sum += input;

        }
        System.out.println("The sum of the Integers list is: " + sum);
    }
}
