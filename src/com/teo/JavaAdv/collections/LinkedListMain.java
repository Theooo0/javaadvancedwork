package com.teo.JavaAdv.collections;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class LinkedListMain {

    public static void main(String[] args) {
        List<String> stringLinkedList = new LinkedList<String>();
        Scanner scn = new Scanner(System.in);
        System.out.println("Introduceti 5 cuvinte: ");

        for (int i = 0; i < 5; i++) {

            stringLinkedList.add(scn.nextLine());
        }
        int firstWordCounter =0;
        for (int i = 0; i < stringLinkedList.size(); i++) {
            if(stringLinkedList.get(i).equals(stringLinkedList.get(0))){
                firstWordCounter ++;
            }

        }
        System.out.println("Primul cuvant din lista " + stringLinkedList.get(0)+
                " , apare de " + firstWordCounter+ " ori.");
    }
}
