package com.teo.JavaAdv.Point2D3D;

import com.teo.JavaAdv.Point2D3D.Point2D;

public class Point3D extends Point2D {
    private Float z;
    private Float[] xyz;

    public Point3D(Float x, Float y, Float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Float getZ() {
        return z;
    }

    public Float[] getXyz() {
        return xyz;
    }

    public void setZ(Float z) {
        this.z = z;
    }

    public void setXYZ(Float[] xyz) {
        this.xyz = xyz;
    }

    public String toString() {
        return "x,y,z";
    }
}

