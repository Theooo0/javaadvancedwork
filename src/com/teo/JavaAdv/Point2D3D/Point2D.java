package com.teo.JavaAdv.Point2D3D;

public class Point2D {

    public Float x;
    public Float y;
    public Float[] xy;

    public Point2D() {

    }

    public Point2D(Float x, Float y) {
        this.x = x;
        this.y = y;

    }

    public Float getX() {
        return x;
    }

    public Float getY() {
        return y;
    }

    public Float[] getXy() {
        return xy;
    }

    public void setX(Float x) {
        this.x = x;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public void setXy(Float[] xy) {
        this.xy = xy;
    }

    public String toString() {
        return "x,y";
    }


}
