package com.teo.JavaAdv.plants;

public class Cactus extends Plant {

    public Cactus(String name, int price) {
        setName(name);
        setAquisitionPrice(price);

    }

    @Override
    int getSellingPrice() {
        return getAquisitionPrice();
    }
}
