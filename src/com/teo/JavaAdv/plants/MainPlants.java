package com.teo.JavaAdv.plants;

public class MainPlants {

    public static void main(String[] args) {
        Monstera m = new Monstera("Monstera minima", 28);
        Rose r = new Rose("Trandafir salbatic", 5);
        Cactus c = new Cactus("Alex", 100);

        System.out.println(m.getPlantInfo() + " with a selling price " + m.getSellingPrice());
        System.out.println(r.getPlantInfo() + " with a selling price " + r.getSellingPrice());
        System.out.println(c.getPlantInfo() + " with a selling price " + c.getSellingPrice());

    }
}
