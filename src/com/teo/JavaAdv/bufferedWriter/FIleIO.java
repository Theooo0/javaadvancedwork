package com.teo.JavaAdv.bufferedWriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FIleIO {
    public static void main(String[] args) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"));
            writer.write("Writing to a file.");
            writer.write("\nHello, this is my first BufferedWriter exemple");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
