package com.teo.JavaAdv.claseanonime;

// Single Abstract Method Interface

// Functional Interface

@FunctionalInterface
public interface OneMethod {
    void theOneMethod();
}
