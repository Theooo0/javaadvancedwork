package com.teo.JavaAdv.school;


import com.teo.JavaAdv.school.enums.Subject;
import com.teo.JavaAdv.school.exceptions.InvalidAgeException;

// extends răspunde/e analog la "is a" ex: Teacher is a Person
public class Teacher extends Person {

    // English Math Chemistry Romanian
    private Subject subject;

    public Teacher(String cnp, String firstName, String lastName, Integer age, String subject) throws InvalidAgeException {
        // super apelează constructorul SUPERclasei Person
        super(cnp, firstName, lastName, age);
        this.subject = Subject.valueOf(subject);
    }

    public Teacher(String cnp, String subject) {
        // super apelează constructorul SUPERclasei Person
        super(cnp);
        this.subject = Subject.valueOf(subject);
    }

    @Override
    public String toString() {
        return "Salut! Eu sunt un profesor de " + subject + ". " + super.toString();
    }
}




/*

Shapes - getArea

Triunghi - baza inaltime

Patrat - baza

dreptunghi - baza inaltime


 */
