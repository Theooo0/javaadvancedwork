package com.teo.JavaAdv.school;


import com.teo.JavaAdv.school.exceptions.InvalidCnpException;

public class Student extends Person{

    private double examGrade;

    public Student(String cnp, String firstName, String lastName, int age, double examGrade) throws InvalidCnpException {
        super(cnp, firstName, lastName, age);
        this.examGrade = examGrade;
    }


    public double getExamGrade() {
        return examGrade;
    }

    public void setExamGrade(double examGrade) {
        this.examGrade = examGrade;
    }



}
