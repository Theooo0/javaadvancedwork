package com.teo.JavaAdv.school.enums;

public enum Subject {

    ENGLISH, MATH, CHEMISTRY, ROMANIAN
}
