package com.teo.JavaAdv.generics;


import com.teo.JavaAdv.generics.gradini.GradinaDeLegume;
import com.teo.JavaAdv.generics.gradini.Livada;
import com.teo.JavaAdv.generics.plante.Leguma;
import com.teo.JavaAdv.generics.plante.Pom;

public class MainGenerics {

    public static void main(String[] args) {



        GradinaDeLegume gradinaDeLegume = new GradinaDeLegume();

        gradinaDeLegume.addElement(new Leguma("spanac", 5));
        gradinaDeLegume.addElement(new Leguma("morcov", 10));

        System.out.println("cea mai grea de ingrijit " + gradinaDeLegume.hardestToCareFor().getDenumire());

        Livada livada = new Livada();

        livada.addElement(new Pom(10,"Stejar"));
        livada.addElement(new Pom(5,"Mesteacan"));
//        livada.addElement(new Leguma("spanac", 5));

        System.out.println("Cel mai greu de ingrijit pom este: " + livada.hardestToCareFor().dificultate);


    }
}
