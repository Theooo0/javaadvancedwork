package com.teo.JavaAdv.animals;


import com.teo.JavaAdv.animals.enums.Species;

import java.util.ArrayList;
import java.util.List;

public abstract class Animal {

    private Species species;

    Animal(Species species) {
        this.species = species;
    }

    public static int totalCount = 0;
    public static List<String> animalsNames = new ArrayList<>();

    abstract public String makeSound();
    abstract public String getName();






    }

