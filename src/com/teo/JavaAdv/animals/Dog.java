package com.teo.JavaAdv.animals;


import com.teo.JavaAdv.animals.enums.Species;

import java.util.Objects;

public class Dog extends Animal  {


    private String name;
    private int id;

    public Dog(String name, int id) {
        super(Species.DOG);
        this.name = name;
        this.id = id;
        totalCount++;
        animalsNames.add(this.name);
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }


    @Override
    public String makeSound() {
        return "Woof";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return id == dog.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }





   // @Override
    public int compareTo(Object o) {
        if (this.getClass()!=o.getClass()){
            return -1;
        }
        Dog dog = (Dog) o;
        if (this.id > dog.id) {
            return 1;
        } else if (this.id < dog.id){
            return -1;
        }
        else {
            return 0;

        }
    }
}

