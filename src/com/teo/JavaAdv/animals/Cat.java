package com.teo.JavaAdv.animals;


import com.teo.JavaAdv.animals.enums.Species;

import java.util.Objects;


public class Cat extends Animal {

    private String name;
    private int id;

    public Cat(String name, int id) {
        super(Species.CAT);
        this.name = name;
        this.id = id;
        totalCount++;
        animalsNames.add(this.name);
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String makeSound() {
        return "Miau";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        // dar daca nu e exact acelasi obiect (are aceeasi zona de memorie)
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return id == cat.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


}
