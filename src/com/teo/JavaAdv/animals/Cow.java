package com.teo.JavaAdv.animals;


import com.teo.JavaAdv.animals.enums.Species;

public class Cow extends Animal {


    private String name;

    Cow(String name) {
        super(Species.COW);
        this.name = name;
        totalCount++;
        animalsNames.add(this.name);
    }

    @Override
    public String getName() {
        return this.name;
    }

   @Override
    public String makeSound() {
       return "Moo";
   }

}
